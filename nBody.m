function [tOut,states] = nBody( FileName,t,dt,AbsTol,RelTol )
%nBody Calculates n-body simulation, according to initial conditions
%supplied
%	FileName=name of .mat file in InitialConditions folder containing
%	system to be simulated. If no FileName specified, SolarSystem10
%	is simulated. Don't use .mat extension in argument.
%	(Optional: t=time to simulate for in seconds, default is
%	100000000, dt=simulation time step in seconds, default is 1000,
%	AbsTol=Absolute error tolerances that apply to the individual
%	components of the solution , RelTol=Relative error tolerance that
%	applies to all components of the solution
global posMatSave DrawAlong DrawAtEnd RGB N Focus
global E GPE KE CentreOfMass %global variables for passing to other 
%functions or for receiveing results to save
warning('off','MATLAB:MKDIR:DirectoryExists')
mkdir('Results');
DrawAlong=false;				% set to true to plot as it goes.
close all
%% Deal with number of input arguments
NArgs=nargin;	%number of input arguments
switch NArgs	% where arguments not given, using default values
	case 0
		FileName = 'SolarSystem10';	% file to read containing initial conditions
		t =100000000;	% simulation time
		dt=1000;		% time step
		AbsTol=1e-8;	%Absolute error tolerances that apply to the individual components of the solution
		RelTol=1e-6;	%Relative error tolerance thata pplies to all components of the solution
	case 1
		t =100000000;
		dt=1000;
		AbsTol=1e-8;
		RelTol=1e-6;
	case 2
		dt=1000;
		AbsTol=1e-8;
		RelTol=1e-6;
	case 3
		AbsTol=1e-8;
		RelTol=1e-6;
	case 4
		RelTol=AbsTol;
	case 5
		% do nothing
	otherwise
		error('nBodyProject:nBody:Arguments', 'Incorrect number of input arguments, must be 0, 1, 2 or 3')
end

load(fullfile('InitialConditions',FileName)); %loads file containing initial conditions
% loads origStates (positions and velocities of each object, in format ...
% ... (x1,y1,z1,xv1,yv1,zv1, x2,y2...)), G (gravitational constant) and ...
% ... mass (of each object, in format: (m1; m2; m3; ...), ...
% ... RGB (colours of each object)

NumOfTimeSteps=floor(t/dt); % so that @outputODE knows how big to make its matrices

odeOptions=odeset('AbsTol',AbsTol,'RelTol',RelTol);	%set tolerence levels for ode45
odeOptions=odeset(odeOptions,'OutputFcn',@outputODE);	%set function to be called after each timestep
[tOut,states]=ode45(@forODE,[0:dt:t],origStates,odeOptions,mass,G);

%% Saving
SaveName=[char(FileName) '_t=' num2str(t) '_dt=' num2str(dt)]; %make a nice filename
SaveName=strrep(SaveName,'.','-') %'.' confuses matlab, so replace with '-'
saveLocation=fullfile('Results',SaveName); %generate file path regardless of Operating system

save(saveLocation,'tOut','posMatSave','RGB','E', 'GPE', 'KE','NumOfTimeSteps','CentreOfMass');
% saves the variables to a .mat file for later plotting

end

