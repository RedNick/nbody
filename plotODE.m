function [] = plotODE( FileName,Focus,Animate,nStart,nEnd )
%plotODE Plot the orbits calculated by nBody
%   FileName = name of .mat file in Results folder containing
%   system to be plotted. If no FileName specified, SolarSystem10_t=100000000_dt=10000
%   is plotted. Don't use .mat extension in argument.
%	(Optional: Focus=number of body to centre on. e.g. for SolarSystem10
%	etc. Focus=4 will output a geocentric plot. Focus=-1 for centre of mass
%	Focus = 0 for centre on 0,0,0. Defaults to -1
%	if Animate>=1, makes an animation with Animate=how many time steps to
%	advance for each frame of animation. If Animate=0, doesn't animate
%	Defaults to false. Defaults to 0. Must be integer.
%	nStart,nEnd=when to start and stop plotting in num of timesteps
%	
close all
NArgs=nargin;
if NArgs==0
	FileName = 'SolarSystem10_t=100000000_dt=10000';
end
load(fullfile('Results',FileName));
switch NArgs
	case 0
		Focus=-1;
		Animate=0;
		nStart=1;
		nEnd=size(posMatSave{1},2);
	case 1
		Focus=-1;
		Animate=0;
		nStart=1;
		nEnd=size(posMatSave{1},2);
	case 2
		Animate=0;
		nStart=1;
		nEnd=size(posMatSave{1},2);
	case 3
		nStart=1;
		nEnd=size(posMatSave{1},2);
	case 4
		nEnd=size(posMatSave{1},2);
	case 5
		% do nothing, all arguments filled
	otherwise
		error('nBodyProject:plotODE:Arguments', 'Incorrect number of input arguments')
end


hold on
if Focus==0			% focus on 0,0,0
	posMatFocus=posMatSave{1}*0;
elseif Focus==-1	% focus on centre of mass
	if (~exist('CentreOfMass','var'))	%CentreOfMass may not be in old results
		error('nBodyProject:plotODE:CentreOfMass', 'CentreOfMass not found in file, please run with Focus>=0 or regenerate results with nBody')
	end
	posMatFocus=CentreOfMass';
else
	posMatFocus=posMatSave{Focus};
end
N=length(posMatSave);
for n=1:N	%for each body
	posMatWorking=posMatSave{n}-posMatFocus;
	X=posMatWorking(1,nStart:nEnd);
	Y=posMatWorking(2,nStart:nEnd);
	Z=posMatWorking(3,nStart:nEnd);
	planetPlotOrbits=plot3(X,Y,Z,'-','Color',RGB(n,:)); %plotting paths
	if Animate==false
		planetPlotPoints=plot3(X(end),Y(end),Z(end),'o','Color',RGB(n,:)); %plotting
		% a circle on each orbit indicating where bodies are instantaneously
	end
end

if Focus==0
	% do nothing
elseif Focus==-1
	planetPlotFocus=plot3(0,0,0,'xk');	%plot x at centre of mass
elseif Animate==true
	%do nothing
else
	planetPlotFocus=plot3(0,0,0,'o','Color',RGB(Focus,:));
	set(planetPlotFocus,'MarkerEdgeColor','k','MarkerFaceColor',RGB(Focus,:));
end

if Animate>=1
	XAnim=zeros(1,N);
	YAnim=zeros(1,N);
	ZAnim=zeros(1,N);
	Names=cell(1,N);
	for n=1:N
		PlotNames{n}=genvarname('AnimationPlot',who); % generate a new name for each plot
		eval([PlotNames{n} '=plot3(XAnim(n),YAnim(n),ZAnim(n),''ko'',''MarkerFaceColor'',RGB(n,:),''XDataSource'',''XAnim(n)'',''YDataSource'',''YAnim(n)'',''ZDataSource'',''ZAnim(n)'');']);
	end
	axis equal
	for timeStep=nStart:Animate:nEnd
		for n=1:N	%for each body
			posMatWorking=posMatSave{n}-posMatFocus;
			XAnim(n)=posMatWorking(1,timeStep);
			YAnim(n)=posMatWorking(2,timeStep);
			ZAnim(n)=posMatWorking(3,timeStep);
			eval(['refreshdata(' PlotNames{n} ',''caller'')'])
		end
		drawnow
		mov(timeStep)=getframe;
	end
	warning('off','MATLAB:MKDIR:DirectoryExists')
else %don't animate
end

normalisedE=(E-E(1))*100/E(1);

figEnergy=figure('position',[10 15 500 700], 'name', 'Energy');
subplot(3,1,1)
Eplot=plot(normalisedE(nStart:nEnd),'r');
ylabel('Error')
xlabel('Number of Time Steps')
title('% Error in total energy')
subplot(3,1,2)
GPEplot=plot(GPE(nStart:nEnd),'g');
ylabel('Gravitational Potential Energy');
xlabel('Number of Time Steps')
title('Gravitational Potential Energy (Joules)');
subplot(3,1,3)
KEplot=plot(KE(nStart:nEnd),'b');
ylabel('Kinetic Energy')
xlabel('Number of Time Steps')
title('Kinetic Energy (Joules)')
fprintf('\nFinished\n')

end

