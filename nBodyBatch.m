%% Batch file
clear all; close all; clc

try
	matlabpool
end
%% Files: FileName, Simulation Time, TimeStep
FileList={'3BodyFigureOf8',100,0.005;'3BodyNew',100,0.005;'3BodyPythag',100,0.005;...
	'EarthMoon',1000000,100;'EarthMoonMoon',1000000,100;'SunEarthMoon',10000000,100;...
	'SolarSystem9noPluto',10000000,1000;'SolarSystem10',10000000,1000;...
	'SolarSystem11',10000000,1000;'SolarSystem12',10000000,1000;...
	'SolarSystem13',10000000,1000;'Apophis',756864000,1000}

% FileList={'Random10',100,0.005;'Random100',100,0.005;'Random1000',100,0.005}
	
parfor index=1:size(FileList,1) %to generate multiple sets of results at once
	FileName=FileList{index,1};
	t=FileList{index,2};
	dt=FileList{index,3};

	nBody(FileName,t,dt);
end
 