function [ status ] = outputODE( time,state,flag,mass,G )
%outputODE Saves energies, centre of mass, positions at each time stop
%   set DrawAlong=true in nBody.m if you want it to plot as nBody
%   calculates results.
global posMatSave planetPlotODE timeStep N
global E GPE KE DrawAlong CentreOfMass TotalMass
switch flag
	case 'init' %ode45 calls this section to initialise the plotting routine
		timeStep=0;
		E=zeros(1,size(state,1));	%for total energy at each time step
		GPE=E;						%gravitational potential energy
		KE=E;						%kinetic energy
		TotalMass=sum(mass);		%for centre of mass calculation
		CentreOfMass=zeros(size(state,1),3);
		posMat=[state(1:6:end,1),state(2:6:end,1),state(3:6:end,1)]; %extract position of each particle
		N=size(posMat,1);		%number of bodies
		posMatSave=cell(N,1);	%creates cell to store positions of each particle throughout time
		if DrawAlong==true %to enable drawing as ode45 calcs, set DrawAlong=true in nBody.m
			rMag=sqrt(sum((posMat).^2,2));
			furthest=max(rMag);		%calculates distance to furthest particle
			limit=furthest*1.5;
			figure('position',[150 150 800 800], 'name', 'Display');
			planetPlotODE=plot3(posMat(:,1),posMat(:,2),posMat(:,3),'ko');
			axis([-1 1 -1 1 -1 1]*limit);
			set(planetPlotODE,'XDataSource','posMat(:,1)','YDataSource','posMat(:,2)','ZDataSource','posMat(:,3)');
		end
		
	case [] %ode45 calls this after every timestep
		T=size(time,2);
		for t=1:T	%this is for ode45 outputting multiple steps at once
			timeStep=timeStep+1;
			posMat=[state(1:6:end,t),state(2:6:end,t),state(3:6:end,t)];	%extract positions of bodies
			CentreOfMass(timeStep,1:3)=(sum(bsxfun(@times,mass,posMat),1))/TotalMass;	%calc centre of mass of system, so plots can centre on it.
			for n=1:N
				posMatSave{n}(1:3,timeStep)=posMat(n,:);	%save each body's position in a cell
			end
			velMat=[state(4:6:end,t),state(5:6:end,t),state(6:6:end,t)]; %extract velocity of each body
			velMag=sum(velMat.^2,2); %calc magnitude of each velocity
			KE(timeStep)=sum(mass.*velMag/2); %calc total Kinetic energy at that time
			gpe=zeros(1,N);
			for index=1:N	%for each body
				r=bsxfun(@minus,posMat(index,:),posMat); %calc position relative to other bodies
				rMag=sqrt(sum((r).^2,2));% calc distance to each body
				rMagInv=1./rMag;
				rMagInv(index)=0;	%set inverse of distance to itself=0 so not affecting sum
				gpe(index)= sum(-G*mass(index)*mass.*rMagInv/2); %calc grav potential energy for each body
			end
			GPE(timeStep)=sum(gpe);		%calc total GPE at that time
			E(timeStep)=KE(timeStep)+GPE(timeStep);		%calc total energy at that time
			
			if DrawAlong==true
				refreshdata(planetPlotODE,'caller')
				drawnow
			end
		end
		status=0;
		
	case 'done'
	otherwise
		fprintf('\nerror\n')
		
end

end

