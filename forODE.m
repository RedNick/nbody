function [ output ] = forODE( ~,input,mass,G )
%forODE Calculates velocity
%   input=(x1,y1,z1,vx1,vy1,vz1, ...)
%	output=(vx1,vy1,vz1,ax1,ay1,az1, ...)
%	x1=x co-ord of particle 1, vx1=x velocity of particle 1, ax1= x accel of particle 1, etc.
pos=[input(1:6:end) input(2:6:end) input(3:6:end)];
N=size(pos,1);
accel=zeros(N,3);
for index=1:N
	r=bsxfun(@minus,pos(index,:),pos);
	rMag=sqrt(sum((r).^2,2));
	accelTemp=-G*bsxfun(@times,bsxfun(@rdivide,r,rMag.^3),mass);
	accelTemp(index,:)=zeros(1,3);
	accel(index,1:3)=sum(accelTemp);
end
output=zeros(6*N,1);
output(1:6:end)=input(4:6:end);
output(2:6:end)=input(5:6:end);
output(3:6:end)=input(6:6:end);
output(4:6:end)=accel(:,1);
output(5:6:end)=accel(:,2);
output(6:6:end)=accel(:,3);
end