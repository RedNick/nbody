close all; clear all;
warning('off','MATLAB:MKDIR:DirectoryExists')
mkdir('InitialConditions');
clc;

%% 2 body, same masses
clear all
G=1;

mass=[1;1];
posA=[1 1 1];
posB=[-1 -1 -1];

velA=[-0.5 0 0];
velB=[0.5 0 0];
origStates=[posA velA posB velB]';
RGB=[1 0 0; 0 1 0];
save('InitialConditions/2BodyEqual','origStates','mass','G','RGB')

%% 2 body, different masses
clear all
G=1;

mass=[2;1];
posA=[1 1 1];
posB=[-1 -1 -1];

velA=[-0.5 0 0];
velB=[0.5 0 0];
origStates=[posA velA posB velB]';
RGB=[1 0 0; 0 1 0];
save('InitialConditions/2BodyNotEqual','origStates','mass','G','RGB')

%% 2 body, different masses, B
clear all
G=1;

mass=[2;1];
posA=[0 1 0];
posB=[0 -2 0];

velA=[0 0 0];
velB=[1 0 0];
origStates=[posA velA posB velB]';
RGB=[1 0 0; 0 1 0];
save('InitialConditions/2BodyNotEqualB','origStates','mass','G','RGB')

%% 3 body, equilateral triangle, pythagorean
clear all
G=1;

mass=[3;4;5];
posA=[1 3 0];
posB=[-2 -1 0];
posC=[1 -1 0];

velA=[0 0 0];
velB=velA; velC=velA;
origStates=[posA velA posB velB posC velC]';
RGB=[1 0 0; 0 1 0; 0 0 1];
save('InitialConditions/3BodyPythag','origStates','mass','G','RGB')

%% 3 body, figure of 8
clear all
G=1;
mass=[1;1;1];

posA=[-0.97000436 0.24308753 0];
posB=-posA;
posC=[0 0 0];

velC=[0.93240737, 0.86473146 0];
velA= -velC/2;
velB=velA;

RGB=[1 0 0; 0 1 0; 0 0 1];
origStates=[posA velA posB velB posC velC]';
save('InitialConditions/3BodyFigureOf8','origStates','mass','G','RGB')

%% 3 body, binary stars + planet
clear all
G=1;

mass=[2;2;0.1];
posA=[1 1 0];
posB=[-1 -1 0];
posC=[0 5 0];

velA=[-0.5 0 0];
velB=[0.5 0 0];
velC=[1 0 0];

origStates=[posA velA posB velB posC velC]';
RGB=[1 0 0; 0 1 0; 0 0 1];
save('InitialConditions/3BodyBinary','origStates','mass','G','RGB')

%% Earth-Moon
clear all
G=6.67300e-11;

massA=5.9736e24;	% earth
posA=[0 0 0];
velA=[0 0 0];

massB=7.3477e22;	% moon
posB=[3.84748e8 0 0];
velB=[0 964 0];

RGB=[0 0.2 1; 0.5 0.5 0.5];
mass=[massA;massB];
origStates=[posA velA posB velB]';
save('InitialConditions/EarthMoon','origStates','mass','G','RGB')

%% Earth-2 Moons (equal & opposite)
clear all
G=6.67300e-11;

massA=5.9736e24;	% earth
posA=[0 0 0];
velA=[0 0 0];

massB=7.3477e22;	% moon
posB=[3.84748e8 0 0];
velB=[0 964 0];

massC=7.3477e22;% moon2
posC=[-3.84748e8 0 0];
velC=[0 -964 0];

RGB=[0 0.2 1; 0.5 0.5 0.5; 1 1 1];
mass=[massA;massB;massC];
origStates=[posA velA posB velB posC velC]';
save('InitialConditions/EarthMoonMoon','origStates','mass','G','RGB')

%% Jupiter Trojans (A.D. 2012-Nov-26 00:00:00.0000)
clear all
G=6.67300e-11;
mass=[];
origStates=[];
RGB=[];

mass=[mass;1.9891e30]; %sun
origStates=[origStates, 0, 0, 0];		%position in cartesian co-ords (km)
origStates=[origStates, 0, 0, 0];		%velocity in cartesian co-ords (km/s)
RGB=[RGB; 255, 179, 16];

mass=[mass;1898.13e24];	%jupiter
origStates=[origStates, 2.524055427301911E+08,  7.123748381375754E+08, -8.606727144898340E+06];
origStates=[origStates, -1.248540616093202E+01,  4.990356644714860E+00,  2.586750244361701E-01];
RGB=[RGB; 205, 133, 63];

mass=[mass;0];	% Asteroid 588 Achilles (1906 TG)
origStates=[origStates, -5.905463249438572E+08  3.834863169162852E+08 -2.315255871884235E+07];
origStates=[origStates, -8.961300604957392E+00 -1.091740865900631E+01 -2.565135461600386E+00];
RGB=[RGB; 255, 0, 0];

mass=[mass;0];	% Asteroid 3317 Paris (1984 KF)
origStates=[origStates, 6.497552190511898E+08, 2.733332081860672E+08, -3.427431866379457E+08];
origStates=[origStates, -4.492089609686130E+00, 1.185280469054626E+01, -2.847850073656260E+00];
RGB=[RGB; 0, 255, 0];

RGB=RGB/255;
origStates=origStates*1000;   %convert Km to m
save('InitialConditions/JupiterTrojans','origStates','mass','G','RGB')
% orbital period =4,333 earth days=374371200 seconds

%% Earth trojan: Asteroid 2010 TK7 (A.D. 2012-Nov-26 00:00:00.0000)
clear all
G=6.67300e-11;
mass=[];
origStates=[];
RGB=[];

mass=[mass;1.9891e30]; %sun
origStates=[origStates, 0, 0, 0];		%position in cartesian co-ords
origStates=[origStates, 0, 0, 0];		%velocity in cartesian co-ords
RGB=[RGB; 255, 179, 16];

mass=[mass;5.9736e24];  %earth
origStates=[origStates, 6.482141753883250E+07,  1.326427950874440E+08, -4.287051814280698E+03];
origStates=[origStates, -2.724225985766603E+01,  1.295891895155441E+01,  6.619894517207385E-04];
RGB=[RGB; 0, 0, 205];

mass=[mass;0];  %asteroid 2010 TK7
origStates=[origStates, 4.243029341207437E+07, 1.276845707443693E+08, -2.161894619169733E+07];
origStates=[origStates, -3.002329530471234E+01, 5.975539915679783E+00, 1.112172177756040E+01];
RGB=[RGB; 200, 200, 200];


RGB=RGB/255;
origStates=origStates*1000;   %convert Km to m
save('InitialConditions/EarthTrojan','origStates','mass','G','RGB')

%% Solar System 10 bodies up to Pluto (A.D. 2012-Nov-26 00:00:00.0000)
clear all
G=6.67300e-11;
mass=[];
origStates=[];
RGB=[];

mass=[mass;1.9891e30]; %sun
origStates=[origStates, 0, 0, 0];		%position in cartesian co-ords
origStates=[origStates, 0, 0, 0];		%velocity in cartesian co-ords
RGB=[RGB; 255, 179, 16];

mass=[mass;3.302e23];   %mercury
origStates=[origStates, -1.423511885582712E+07,  4.462950252990882E+07,  4.952618132270868E+06];
origStates=[origStates, -5.618796146926461E+01, -1.300286294193999E+01,  4.093000518339627E+00];
RGB=[RGB; 226, 226, 226];

mass=[mass;48.685e23];  %venus
origStates=[origStates, -1.065528698297825E+08,  1.396002772149041E+07,  6.340798422150703E+06];
origStates=[origStates, -4.738541234789772E+00, -3.488076781903284E+01, -2.044712408193314E-01];
RGB=[RGB; 222, 184, 135];

mass=[mass;5.9736e24];  %earth
origStates=[origStates, 6.482141753883250E+07,  1.326427950874440E+08, -4.287051814280698E+03];
origStates=[origStates, -2.724225985766603E+01,  1.295891895155441E+01,  6.619894517207385E-04];
RGB=[RGB; 0, 0, 205];

mass=[mass;6.4185e23];  %mars
origStates=[origStates, 1.013391621577575E+08, -1.841941159251069E+08, -6.347324298263528E+06];
origStates=[origStates, 2.214613232458264E+01,  1.375531651321878E+01, -2.555521753525583E-01];
RGB=[RGB; 225, 0, 0];

mass=[mass;1898.13e24]; %jupiter
origStates=[origStates, 2.524055427301911E+08,  7.123748381375754E+08, -8.606727144898340E+06];
origStates=[origStates, -1.248540616093202E+01,  4.990356644714860E+00,  2.586750244361701E-01];
RGB=[RGB; 205, 133, 63];

mass=[mass;5.68319e26]; %saturn
origStates=[origStates, -1.223919288716288E+09, -7.999406851712474E+08,  6.263845068254739E+07];
origStates=[origStates, 4.750802567706457E+00, -8.105802281988492E+00, -4.769044716826750E-02];
RGB=[RGB; 255, 215, 0];

mass=[mass;86.8103e24]; %uranus
origStates=[origStates, 2.978383593357699E+09,  3.649340149933032E+08, -3.722361724428883E+07];
origStates=[origStates, -8.878224370933605E-01,  6.445008239043002E+00,  3.553843799634719E-02];
RGB=[RGB; 135, 206, 250];

mass=[mass;102.41e24];  %neptune
origStates=[origStates, 3.965714114400936E+09, -2.097945788752183E+09, -4.817134235780217E+07];
origStates=[origStates, 2.495371171638834E+00,  4.838605576860306E+00, -1.577202523723045E-01];
RGB=[RGB; 30, 144, 255];

mass=[mass;1.314e22];   %pluto
origStates=[origStates, 7.473177156865152E+08, -4.770767178131273E+09,  2.944420902789887E+08];
origStates=[origStates, 5.475416485960205E+00, -2.216923235388973E-01, -1.549159437104290E+00];
RGB=[RGB; 210, 105, 30];

RGB=RGB/255;
origStates=origStates*1000;   %convert Km to m
save('InitialConditions/SolarSystem10','origStates','mass','G','RGB')


%% Solar System 12 bodies (incl. Ceres) up to Eris (A.D. 2012-Nov-26 00:00:00.0000)
clear all
G=6.67300e-11;
mass=[];
origStates=[];
RGB=[];

mass=[mass;1.9891e30]; %sun
origStates=[origStates, 0, 0, 0];		%position in cartesian co-ords
origStates=[origStates, 0, 0, 0];		%velocity in cartesian co-ords
RGB=[RGB; 255, 179, 16];

mass=[mass;3.302e23];   %mercury
origStates=[origStates, -1.423511885582712E+07,  4.462950252990882E+07,  4.952618132270868E+06];
origStates=[origStates, -5.618796146926461E+01, -1.300286294193999E+01,  4.093000518339627E+00];
RGB=[RGB; 150, 150, 150];

mass=[mass;48.685e23];  %venus
origStates=[origStates, -1.065528698297825E+08,  1.396002772149041E+07,  6.340798422150703E+06];
origStates=[origStates, -4.738541234789772E+00, -3.488076781903284E+01, -2.044712408193314E-01];
RGB=[RGB; 222, 184, 135];

mass=[mass;5.9736e24];  %earth
origStates=[origStates, 6.482141753883250E+07,  1.326427950874440E+08, -4.287051814280698E+03];
origStates=[origStates, -2.724225985766603E+01,  1.295891895155441E+01,  6.619894517207385E-04];
RGB=[RGB; 0, 0, 205];

mass=[mass;6.4185e23];  %mars
origStates=[origStates, 1.013391621577575E+08, -1.841941159251069E+08, -6.347324298263528E+06];
origStates=[origStates, 2.214613232458264E+01,  1.375531651321878E+01, -2.555521753525583E-01];
RGB=[RGB; 225, 0, 0];

mass=[mass;9.43e20];		%ceres
origStates=[origStates, 5.843152536796071E+07  3.973950605884629E+08  1.711381113684478E+06];
origStates=[origStates, -1.809174523475683E+01  1.333100984070475E+00  3.377598259244718E+00];
RGB=[RGB; 200, 200, 200];

mass=[mass;1898.13e24]; %jupiter
origStates=[origStates, 2.524055427301911E+08,  7.123748381375754E+08, -8.606727144898340E+06];
origStates=[origStates, -1.248540616093202E+01,  4.990356644714860E+00,  2.586750244361701E-01];
RGB=[RGB; 205, 133, 63];

mass=[mass;5.68319e26]; %saturn
origStates=[origStates, -1.223919288716288E+09, -7.999406851712474E+08,  6.263845068254739E+07];
origStates=[origStates, 4.750802567706457E+00, -8.105802281988492E+00, -4.769044716826750E-02];
RGB=[RGB; 255, 215, 0];

mass=[mass;86.8103e24]; %uranus
origStates=[origStates, 2.978383593357699E+09,  3.649340149933032E+08, -3.722361724428883E+07];
origStates=[origStates, -8.878224370933605E-01,  6.445008239043002E+00,  3.553843799634719E-02];
RGB=[RGB; 135, 206, 250];

mass=[mass;102.41e24];  %neptune
origStates=[origStates, 3.965714114400936E+09, -2.097945788752183E+09, -4.817134235780217E+07];
origStates=[origStates, 2.495371171638834E+00,  4.838605576860306E+00, -1.577202523723045E-01];
RGB=[RGB; 30, 144, 255];

mass=[mass;1.314e22];   %pluto
origStates=[origStates, 7.473177156865152E+08, -4.770767178131273E+09,  2.944420902789887E+08];
origStates=[origStates, 5.475416485960205E+00, -2.216923235388973E-01, -1.549159437104290E+00];
RGB=[RGB; 210, 105, 30];

mass=[mass;1.7e22];		%eris
origStates=[origStates, 1.303570466736751E+10  5.267628635065827E+09 -3.277854651182829E+09];
origStates=[origStates, -5.876384738334303E-01  1.591436737929802E+00  1.568606051113592E+00];
RGB=[RGB; 0, 0, 0];

RGB=RGB/255;
origStates=origStates*1000;   %convert Km to m
save('InitialConditions/SolarSystem12','origStates','mass','G','RGB')

%% Apophis' close pass of earth (A.D. 2012-Nov-26 00:00:00.0000)
clear all
G=6.67300e-11;
mass=[];
origStates=[];
RGB=[];

mass=[mass;1.9891e30]; %sun
origStates=[origStates, 0, 0, 0];		%position in cartesian co-ords
origStates=[origStates, 0, 0, 0];		%velocity in cartesian co-ords
RGB=[RGB; 255, 179, 16];

mass=[mass;3.302e23];   %mercury
origStates=[origStates, -1.423511885582712E+07,  4.462950252990882E+07,  4.952618132270868E+06];
origStates=[origStates, -5.618796146926461E+01, -1.300286294193999E+01,  4.093000518339627E+00];
RGB=[RGB; 150, 150, 150];

mass=[mass;48.685e23];  %venus
origStates=[origStates, -1.065528698297825E+08,  1.396002772149041E+07,  6.340798422150703E+06];
origStates=[origStates, -4.738541234789772E+00, -3.488076781903284E+01, -2.044712408193314E-01];
RGB=[RGB; 222, 184, 135];

mass=[mass;5.9736e24];  %earth
origStates=[origStates, 6.482141753883250E+07,  1.326427950874440E+08, -4.287051814280698E+03];
origStates=[origStates, -2.724225985766603E+01,  1.295891895155441E+01,  6.619894517207385E-04];
RGB=[RGB; 0, 0, 205];

mass=[mass;6.4185e23];  %mars
origStates=[origStates, 1.013391621577575E+08, -1.841941159251069E+08, -6.347324298263528E+06];
origStates=[origStates, 2.214613232458264E+01,  1.375531651321878E+01, -2.555521753525583E-01];
RGB=[RGB; 225, 0, 0];

mass=[mass;1898.13e24]; %jupiter
origStates=[origStates, 2.524055427301911E+08,  7.123748381375754E+08, -8.606727144898340E+06];
origStates=[origStates, -1.248540616093202E+01,  4.990356644714860E+00,  2.586750244361701E-01];
RGB=[RGB; 205, 133, 63];

mass=[mass;5.68319e26]; %saturn
origStates=[origStates, -1.223919288716288E+09, -7.999406851712474E+08,  6.263845068254739E+07];
origStates=[origStates, 4.750802567706457E+00, -8.105802281988492E+00, -4.769044716826750E-02];
RGB=[RGB; 255, 215, 0];

mass=[mass;86.8103e24]; %uranus
origStates=[origStates, 2.978383593357699E+09,  3.649340149933032E+08, -3.722361724428883E+07];
origStates=[origStates, -8.878224370933605E-01,  6.445008239043002E+00,  3.553843799634719E-02];
RGB=[RGB; 135, 206, 250];

mass=[mass;102.41e24];  %neptune
origStates=[origStates, 3.965714114400936E+09, -2.097945788752183E+09, -4.817134235780217E+07];
origStates=[origStates, 2.495371171638834E+00,  4.838605576860306E+00, -1.577202523723045E-01];
RGB=[RGB; 30, 144, 255];

mass=[mass;2.7e10];     %apophis
origStates=[origStates, 4.914379966297780E+07  1.273576909049545E+08 -5.567428106252837E+06];
origStates=[origStates, -2.652520970494373E+01  1.658342219880887E+01 -1.517214881559788E+00];
RGB=[RGB; 0, 0, 0];

mass=[mass;734.9e20];	% moon
origStates=[origStates, 6.514946885522501E+07,  1.328779174250757E+08,  8.683039954130196E+03];
origStates=[origStates, -2.778897549610618E+01,  1.376299625243174E+01, -8.314245181511830E-02];
RGB=[RGB; 150, 150, 150];

RGB=RGB/255;
origStates=origStates*1000;   %convert Km to m

% Track=4; %to track earth
save('InitialConditions/Apophis','origStates','mass','G','RGB')
%try 24 years
t=24*365*24*60*60;
t=756864000;

%try plotting 510,000,000 to 520,000,000 divided by dt (eg 1000: 510,000 to
%520,000)

%% Sun Earth Moon (A.D. 2012-Nov-26 00:00:00.0000)
clear all
G=6.67300e-11;
RGB=[];

massA=1.9891e30;	% sun
posA=[0 0 0];
velA=[0 0 0];
RGB=[RGB; 255, 179, 16];

massB=5.9736e24;	% earth
posB=[6.482141753883250E+07,  1.326427950874440E+08, -4.287051814280698E+03];
velB=[-2.724225985766603E+01,  1.295891895155441E+01,  6.619894517207385E-04];
RGB=[RGB; 0, 0, 205];

massC=734.9e20;	% moon
posC=[6.514946885522501E+07,  1.328779174250757E+08,  8.683039954130196E+03];
velC=[-2.778897549610618E+01,  1.376299625243174E+01, -8.314245181511830E-02];
RGB=[RGB; 150, 150, 150];
mass=[massA;massB;massC];

RGB=RGB/255;
origStates=[posA velA posB velB posC velC]'*1000;   %convert Km to m
save('InitialConditions/SunEarthMoon','origStates','mass','G','RGB')

%% Random generator (10)
clear all
G=1;

pos=2*abs(randn(10,3));
vel=randn(10,3);
mass=abs(randn(10,1));


origStates=[pos vel];
origStates=origStates(:);
RGB=rand(10,3);
save('InitialConditions/Random10','origStates','mass','G','RGB')

%% Random generator (100)
clear all
G=1;

pos=2*abs(randn(100,3));
vel=randn(100,3);
mass=abs(randn(100,1));


origStates=[pos vel];
origStates=origStates(:);
RGB=rand(10,3);
save('InitialConditions/Random100','origStates','mass','G','RGB')

%% Random generator (1000)
clear all
G=1;

pos=2*abs(randn(1000,3));
vel=randn(1000,3);
mass=abs(randn(1000,1));


origStates=[pos vel];
origStates=origStates(:);
RGB=rand(1000,3);
save('InitialConditions/Random1000','origStates','mass','G','RGB')
