Louis Nicolas Haworth Redfern

Tested on 2012b, 64bit
Due to possibly large data sets, please use a 64 bit version of MATLAB,
32 bit seems unable to plot 'Apophis_t=756864000_dt=1000.mat' for example

Example:
in command window run (I did want to include a pre calculated one, but it was too big for moodle):
nBody('Apophis',520864000,1000);
(this will take a while...) then to plot:
plotODE('Apophis_t=520864000_dt=1000',4,10,515000,520000)
%zoom in on earth (blue) and wait for Apophis to fly past
___________________________________________________________________________
forODE.m
Function called by ode45 to calculate RHS of differential equations
Don't call on its own.
___________________________________________________________________________
nBody.m
Main funcion, Calculates paths of bodies, according to initial conditions supplied,
and saves them in a .mat file in "Results"
Call via command window or nBodyBatch.m
Form: [tOut,states] = nBody( FileName,t,dt,AbsTol,RelTol ) all arguments optional

FileName=name of .mat file in InitialConditions folder containing
system to be simulated. If no FileName specified, SolarSystem10
is simulated. Don't use .mat extension in argument.
t=time to simulate for in seconds, default is 100000000
dt=simulation time step in seconds, default is 1000,
AbsTol=Absolute error tolerances that apply to the individual
components of the solution
RelTol=Relative error tolerance that applies to total of all components of the solution

Alter DrawAlong=false; to true in the function for plotting as ode45 calculates
___________________________________________________________________________
nBodyBatch.m			
Calculates movements for a number of systems in parallel.

___________________________________________________________________________
outputODE.m
Called by ode45 after every step, calculates energies and puts positions in
a nicer format
___________________________________________________________________________
plotODE.m
Plotting function
plotODE( FileName,Focus,Animate,nStart,nEnd )
Plot the orbits calculated by nBody
FileName = name of .mat file in Results folder containing system to be plotted.
If no FileName specified, SolarSystem10_t=100000000_dt=10000 is plotted.
Don't use .mat extension in argument.
Focus=number of body to centre on. e.g. for SolarSystem10 etc.Focus=4 will
output a geocentric plot. Focus=-1 for centre of mass, Focus = 0 for centre on 0,0,0. Defaults to -1
if Animate>=1, makes an animation with Animate=how many time steps to advance
for each frame of animation. If Animate=0, doesn't animate. Defaults to 0. Must be integer.
nStart,nEnd=when to start and stop plotting in num of timesteps
	